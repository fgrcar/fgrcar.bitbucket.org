
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var globalEhr = "";

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var generiraniEhr = [' ', ' ',' '];
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	  var ehrId = "";
	  var imena = ['Zelo', 'Mladi', 'Starejša'];
	  var priimki = ['Bolan', 'Hokejist', 'Gospa'];
	  var dob = ['1968-06-03', '1997-10-05', '1947-11-23'];
	  var teza = ['75', '80', '59', '74', '79', '58.5', '72.5', '79.5', '60.5'];
	  var temp = ['38.3', '36.5', '37.3', '36.6', '36.6', '36.6', '39.2', '36.5', '37.2'];
	  var datumi = ['2017-04-03', '2016-10-05', '2017-01-05', '2017-04-10', '2016-11-06', '2017-01-15', '2017-04-20', '2017-01-04', '2017-02-01'];
	  var tlak1 = ['140', '120', '141', '130', '123', '130', '125', '125', '131'];
	  var tlak2 = ['95', '81', '94', '90', '81', '87', '86', '83', '89'];
	  var srcniUtrip = ['120', '70', '100', '114', '73', '90', '95', '72', '89'];
	  //console.log(imena[0]);
	  var ime = imena[stPacienta-1];
	  var priimek = priimki[stPacienta-1];
	  var datumRojstva = dob[stPacienta-1];
	  sessionId = getSessionId();
	  $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   console.log(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
		var stop = (stPacienta-1)+6;
		for(var i=(stPacienta-1); i<=stop; i = i+3){
			sessionId = getSessionId();
			//var ehrId = globalEhr;
			var datumInUra = datumi[i];
			var telesnaTeza = teza[i];
			var telesnaTemperatura = temp[i];
			var sistolicniKrvniTlak = tlak1[i];
			var diastolicniKrvniTlak = tlak2[i];
			var pulz = srcniUtrip[i];
			var merilec = "Mici";
			console.log(i);
			if (!ehrId || ehrId.trim().length == 0) {
				
			} else {
				$.ajaxSetup({
				    headers: {"Ehr-Session": sessionId}
				});
				var podatki = {
				    "ctx/language": "en",
				    "ctx/territory": "SI",
				    "ctx/time": datumInUra,
				    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
				   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
				    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
				    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
				    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
				    "vital_signs/pulse:0/any_event:0/rate|magnitude":	pulz,
					"vital_signs/pulse:0/any_event:0/rate|unit":	"/min"
				};
				var parametriZahteve = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    committer: merilec
				};
				$.ajax({
				    url: baseUrl + "/composition?" + $.param(parametriZahteve),
				    type: 'POST',
				    contentType: 'application/json',
				    data: JSON.stringify(podatki),
				    success: function (res) {
				        
				    },
				    error: function(err) {
	
				    }
				});
			}
		}
		if(stPacienta == 1){
			generiraniEhr[stPacienta-1] = ehrId;
			generirajPodatke(2);
		} else if(stPacienta == 2){
			generiraniEhr[stPacienta-1] = ehrId;
			generirajPodatke(3);
		} else if(stPacienta == 3){
			generiraniEhr[stPacienta-1] = ehrId;
			alert("Generirani EHR (kopiraj v beležko!)\n" + generiraniEhr[0] + "\n" + generiraniEhr[1] + "\n" + generiraniEhr[2]); 
		}
		
  //return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function dolociOsebo(){
	globalEhr = $("#glavniEHR").val();
	sessionId = getSessionId();
	$("#preberiGlavnoSporocilo").html("");
	$("#imeOsebe").html("");
	$("#patient-name").html("");
	$(".patient-age").html("");
	$(".patient-dob").html("");
	$("#preberiMeritveVitalnihZnakovSporocilo").html("");
	$("#rezultatMeritveVitalnihZnakov").html("");
	$("#rezultatMeritveVitalnihZnakov2").html("");
	$("#dodajMeritveAlergijZnakovSporocilo").html("");
	$("#preberiSporocilo2").html("");
	$("ul.allergies").html("");
	$("#alergijeText").html("");
	var ehrId = globalEhr;

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiGlavnoSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
		    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		    type: 'GET',
		    headers: {
		        "Ehr-Session": sessionId
		    },
		    success: function (data) {
		        var party = data.party;
		        $("#imeOsebe").html('<font size="5">' + 'Izbrana oseba: ' + party.firstNames + ' ' + party.lastNames + '</font>');
		        $("#preberiGlavnoSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Oseba izbrana" + ".</span>");
		    },
		    error: function(err) {
		            	$("#preberiGlavnoSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function pokaziOsebo() {
	sessionId = getSessionId();

	//var ehrId = $("#preberiEHRid").val();
	var ehrId = globalEhr;

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim izberite osebo (na začetku strani)!");
	} else {
		$.ajax({
		    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		    type: 'GET',
		    headers: {
		        "Ehr-Session": sessionId
		    },
		    success: function (data) {
		        var party = data.party;

		        $("#patient-name").html(party.firstNames + ' ' + party.lastNames);
		
		        var age = getAge(formatDateUS(party.dateOfBirth));
		        $(".patient-age").html("Starost: " + age);
		
		        var date = new Date(party.dateOfBirth);
		        var stringDate =  monthNames[date.getMonth()] +'. '+ date.getDate() + ', ' + date.getFullYear();
		        $(".patient-dob").html("Datum rojstva: " + stringDate);
		    }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = globalEhr;
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var pulz = $("#dodajPulz").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim izberite osebo (na začetku strani)!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/pulse:0/any_event:0/rate|magnitude":	pulz,
			"vital_signs/pulse:0/any_event:0/rate|unit":	"/min"
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function dodajAlergije() {
	sessionId = getSessionId();

	//var ehrId = $("#dodajAlergijskoEHR").val();
	var ehrId = globalEhr;
	var datumInUra = $("#dodajAlergijskoDatumInUra").val();
	var alergije = $("#nazivAlergije").val();
	var merilec = $("#dodajAlergijskoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveAlergijZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim izberite osebo (na začetku strani)!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "allergies/adverse_reaction_-_allergy:0/substance_agent": alergije
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Allergies',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveAlergijZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveAlergijZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = globalEhr;
	var tip = $("#preberiTipZaVitalneZnake").val();
	$("#preberiMeritveVitalnihZnakovSporocilo").html("");

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim izberite osebo (na začetku strani)!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
						    	$("#rezultatMeritveVitalnihZnakov2").html("");
						    	if (res.length > 0){
								new Morris.Line({
								  element: 'rezultatMeritveVitalnihZnakov2',
								  data: res.reverse(),
								  xkey: 'time',
								  ykeys: ['temperature'],
								  labels: ['Vrednost'],
								  hideHover: true,
	                    		  lineColors: ['#4500D1'],
	                    		  xLabelMargin: 5,
								  resize: true,
								  xLabelFormat: function (x) {
									var date = new Date(x);
									return (date.getDate() + '-' + monthNames[date.getMonth()]);
			                	  },
			                     dateFormat: function (x) {
									return (formatDate(x, false));
								}
								});
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
						    url: baseUrl + "/view/" + ehrId + "/" + "weight",
						    type: 'GET',
						    headers: {"Ehr-Session": sessionId},
						    success: function (res) {
						    	$("#rezultatMeritveVitalnihZnakov2").html("");
						    	if (res.length > 0){
								new Morris.Area({
								  element: 'rezultatMeritveVitalnihZnakov2',
								  data: res.reverse(),
								  xkey: 'time',
								  ykeys: ['weight'],
								  labels: ['Teža (kg)'],
								  hideHover: 'true',
	                    		  lineColors: ['#297A00'],
	                    		  xLabelMargin: 5,
								  resize: true,
								  xLabelFormat: function (x) {
										var date = new Date(x);
										return (date.getDate() + '-' + monthNames[date.getMonth()]);
			                		  },
					                  dateFormat: function (x) {
									  return (formatDate(x, false));
									  }
								});
						    }	else {
							    $("#preberiMeritveVitalnihZnakovSporocilo").html(
		                    "<span class='obvestilo label label-warning fade-in'>" +
		                    "Ni podatkov!</span>");
							    	}
							    },
							    error: function() {
							    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
		                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		                  JSON.parse(err.responseText).userMessage + "'!");
							    }
					});
				} else if (tip == "krvni tlak") {
					$.ajax({
						    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
						    type: 'GET',
						    headers: {"Ehr-Session": sessionId},
						    success: function (res) {
						    	$("#rezultatMeritveVitalnihZnakov2").html("");
						    	if (res.length > 0){
									new Morris.Area({
									  element: 'rezultatMeritveVitalnihZnakov2',
									  data: res.reverse(),
									  xkey: 'time',
									  ykeys: ['systolic', 'diastolic'],
									  labels: ['Systolic', 'Diastolic'],
									  hideHover: 'true',
		                    		  barColors: ['#CCFF66', '#FF0000'],
		                    		  xLabelMargin: 5,
									  resize: true,
									  xLabelFormat: function (x) {
										var date = new Date(x);
										return (date.getDate() + '-' + monthNames[date.getMonth()]);
			                		  },
					                  dateFormat: function (x) {
									  return (formatDate(x, false));
									  }
									});
						    }	else {
							    $("#preberiMeritveVitalnihZnakovSporocilo").html(
		                    "<span class='obvestilo label label-warning fade-in'>" +
		                    "Ni podatkov!</span>");
							    	}
							    },
							    error: function() {
							    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
		                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		                  JSON.parse(err.responseText).userMessage + "'!");
							    }
					});
				} else if(tip == "srčni utrip"){
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "pulse",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
						    	$("#rezultatMeritveVitalnihZnakov2").html("");
						    	if (res.length > 0){
								new Morris.Line({
								  element: 'rezultatMeritveVitalnihZnakov2',
								  data: res.reverse(),
								  xkey: 'time',
								  ykeys: ['pulse'],
								  labels: ['Utrip /min'],
								  hideHover: true,
	                    		  lineColors: ['#FF0000'],
	                    		  xLabelMargin: 5,
								  resize: true,
								  xLabelFormat: function (x) {
									var date = new Date(x);
									return (date.getDate() + '-' + monthNames[date.getMonth()]);
			                	  },
			                     dateFormat: function (x) {
									return (formatDate(x, false));
								}
								});
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function pokaziAlergije() {
	sessionId = getSessionId();

	var ehrId = globalEhr;
	$("#preberiSporocilo2").html("");
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo2").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim izberite osebo (na začetku strani)!");
	} else {
		$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/allergy",
		    type: 'GET',
		    headers: {
		        "Ehr-Session": sessionId
		    },
		    success: function (res) {
		    	$("ul.allergies").html("");
		    	$("#alergijeText").html("");
		    	if(res.length == 0){
		    		$('ul.allergies').append('Oseba nima alergij');
		    	} else{
		    		$('#alergijeText').append('<b>Alergije danega bolnika:');
			        for(var i=0; i<res.length; i++){
			            $('ul.allergies').append('<li>'+res[i].agent+'</li>');
			        }
		    	}
		    }
		});
	}
}

function pokaziJed(){
	$.ajax({
		    url: "https://api.edamam.com/search?q=chicken&app_id=20acfadd&app_key=daffd44baccb7679d602ab8270e143ba&from=0&to=3&calories=gte%2050,%20lte%201000&health=alcohol-free",
		    type: "GET",
		    headers: {
                "ContentType": "application/json",
                "Accept": "application/json"
            },
		    success: function (res) {
		    	console.log(res.hits[0].recipe);
		    	$('#slikaJedi').append('<img src="' + res.hits[0].recipe.image +' " alt="jed">');
		    	$('#naslovJedi').append('<b>' + res.hits[0].recipe.label + '</b>');
		    	for(var i=0; i<res.hits[0].recipe.ingredientLines.length; i++){
			            $('ul.sestavine').append('<li>'+res.hits[0].recipe.ingredientLines[i]+'</li>');
			    }
		    }
	});
}	

 // Helper functions (dates)

    function getAge(dateString) {
        var now = new Date();
        var today = new Date(now.getYear(), now.getMonth(), now.getDate());

        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();

        var dob = new Date(dateString.substring(6, 10),
                dateString.substring(0, 2) - 1,
            dateString.substring(3, 5)
        );

        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";


        var yearAge = yearNow - yearDob;

        if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
        else {
            yearAge--;
            var monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
        else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };

        if (age.years > 1) yearString = " let";
        else yearString = " leto";
        if (age.months > 1) monthString = " mesecev";
        else monthString = " mesec";
        if (age.days > 1) dayString = " dni";
        else dayString = " dan";


        if ((age.years > 0) && (age.months > 0) && (age.days > 0))
            ageString = age.years + yearString + " " + age.months + monthString;// + ", and " + age.days + dayString + " old";
        else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
            ageString = age.days + dayString + " old";
        else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
            ageString = age.years + yearString;// + " old. Happy Birthday!";
        else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
            ageString = age.years + yearString + " and " + age.months + monthString;// + " old";
        else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
            ageString = age.months + monthString; // + " and " + age.days + dayString + " old";
        else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
            ageString = age.years + yearString;// + " and " + age.days + dayString + " old";
        else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
            ageString = age.months + monthString;// + " old";
        else ageString = "Oops! Could not calculate age!";

        return ageString;
    }

    function formatDate(date, completeDate) {

        var d = new Date(date);

        var curr_date = d.getDate();
        curr_date = normalizeDate(curr_date);

        var curr_month = d.getMonth();
        curr_month++;
        curr_month = normalizeDate(curr_month);

        var curr_year = d.getFullYear();

        var curr_hour = d.getHours();
        curr_hour = normalizeDate(curr_hour);

        var curr_min = d.getMinutes();
        curr_min = normalizeDate(curr_min);

        var curr_sec = d.getSeconds();
        curr_sec = normalizeDate(curr_sec);

        var dateString, monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        if (completeDate){
            dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year + " at " + curr_hour + ":" + curr_min; // + ":" + curr_sec;
        }
        else dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year;

        return dateString;

    }

    function formatDateUS(date) {
        var d = new Date(date);

        var curr_date = d.getDate();
        curr_date = normalizeDate(curr_date);

        var curr_month = d.getMonth();
        curr_month++;
        curr_month = normalizeDate(curr_month);

        var curr_year = d.getFullYear();

        return curr_month + "-" + curr_date + "-" + curr_year;

    }

    function getAgeInYears(dateOfBirth) {
        var dob = new Date(dateOfBirth);
        var timeDiff = Math.abs(Date.now() - dob.getTime());
        return Math.floor(timeDiff / (1000 * 3600 * 24 * 365));
    }

    function normalizeDate(el) {
        el = el + "";
        if (el.length == 1) {
            el = "0" + el;
        }
        return el;
}

$(document).ready(function() {
	$('#izbiraOsebe').change(function() {
		$("#glavnoSporocilo").html("");
		$("#glavniEHR").val($(this).val());
	});
});